import React, {useState, useEffect} from 'react';
import { Link,makeStyles, Container, Typography } from '@material-ui/core';
import { getEmployee } from './helper';

const useStyle = makeStyles(() => ({
  home: {
    marginTop: "50px",
  },
  overviewContent: {
    textAlign: "left",
    marginTop: "20px",
  }
}));

const setMap = (hash, val) => {
  let tempAll = hash;
  tempAll[val] = true;
  return tempAll;
};

const Overview = props => {
  const style = useStyle();
  const [allEmployee, setAllEmployee] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const { match: { params: { name } }, location: { state } } = props;

  const getAllEmployee = name => {
    setIsLoading(true);
    getEmployee(name).then( resp => {
      const { data } = resp;
      if (data.length > 1) {
        let sub = data[1]['direct-subordinates'];
        sub.forEach(val => {
          if (!allEmployee[val]) {
            setAllEmployee({...setMap(allEmployee, val)});
            getAllEmployee(val);
          }
        });
      }
    }).catch(err => {
      let {response: {status}} = err;
      window.location = '/';
    }).then(() => {
      setIsLoading(false);
    });
  };

  useEffect(() => {
    if(state != null) {
      state.subOrdinator.forEach( val => {
        setAllEmployee({...setMap(allEmployee, val)});
        getAllEmployee(val);
      });
    } else {
      setAllEmployee({...setMap(allEmployee, name)});
      getAllEmployee(name);
    }
  }, []);
  const employeeName = `Subordinates of Employee ${name}:`;
  const subOrdinateShow = Object.keys(allEmployee).filter(currName => currName !== name).map(val => (
    <li>
      <Link href={`/${val}`} color="inherit">
        {val}
      </Link>
    </li>
  ));
  const loadingShow = isLoading && (
    <li>
      ...
    </li>
  );

  return(
    <div
      className={style.home}
    >
      <Typography
        variant="h4"
        gutterBottom
      >
        Employee Overview
      </Typography>
      <Container
        maxWidth="md"
        className={style.overviewContent}>
        <Typography variant="h6">
          {employeeName}
        </Typography>
        <ul>
          {subOrdinateShow}
          {loadingShow}
        </ul>
      </Container>
    </div>
  );
}

export default  Overview;
