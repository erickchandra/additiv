import Axios from "axios";

export const  getEmployee = (employeeName) => {
  return Axios.get(`http://api.additivasia.io/api/v1/assignment/employees/${employeeName}`);
};
