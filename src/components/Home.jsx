import React, {useState} from 'react';
import TextField from '@material-ui/core/TextField';
import { Button, makeStyles, ButtonGroup, Typography } from '@material-ui/core';
import { getEmployee } from './helper';

const useStyle = makeStyles(() => ({
  home: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "80vh",
  },
  content: {
    flex: "1",
  },
  errorMessage: {
    color: "red",
  },
  searchButton: {
    marginLeft: 5,
  }
}));

const Home = props => {
  const style = useStyle();
  const [isLoading, setIsLoading] = useState(false);
  const [employeeName, setEmployeeName] = useState("");
  const [errorMessage, setErrorMessage] = useState('');

  const buttonContent = isLoading ? "Loading" : "Search" ;

  const findUser = () => {
    setIsLoading(true);
    getEmployee(employeeName).then( resp => {
        const { data } = resp;
        props.history.push({
          pathname: `/${employeeName}`,
          state: {
            role: data[0],
            subOrdinator: data[1]['direct-subordinates'],
          },
        });
      }).catch( err => {
        const {response: {status}} = err;
        if(status === 404) {
          setErrorMessage("user not found");
        } else {
          setErrorMessage("undefined error");
        }
      }).then(() => {
        setIsLoading(false);
      });
  };

  return(
    <div
      className={style.home}
    >
      <div className={style.content}>
        <Typography
          variant="h4"
          gutterBottom
        >
          Employee Explorer
        </Typography>
        <TextField
          error={errorMessage !== ''}
          id="name"
          label=""
          variant="outlined"
          color="primary"
          size="small"
          disabled={isLoading}
          onChange={e => setEmployeeName(e.target.value)}
        />
        <Button
          disabled={isLoading}
          size="large"
          color="primary"
          disableElevation
          variant="contained"
          className={style.searchButton}
          onClick={() => findUser()}
        >
          {buttonContent}
        </Button>
        <Typography
          variant="subtitle2"
          className={style.errorMessage}
        >
          {errorMessage}
        </Typography>
      </div>
    </div>
  );
}

export default  Home;
