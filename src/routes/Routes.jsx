import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from '../components/Home';
import Overview from '../components/Overview';
import { Container, makeStyles } from '@material-ui/core';

const useStyle = makeStyles(() => ({
  container: {
    padding: '20px',
    backgroundColor: '#FFF',
    height: '100vh'
  }
}));

export const ALL_ROUTES = [
  {
    id: 'home',
    path: '/',
    component: Home,
    onMenu: true
  },
  {
    id: 'overview',
    path: '/:name',
    component: Overview,
    onMenu: false
  }
];

const Routes = () => {
  const style= useStyle();
  const allRoutes = ALL_ROUTES.map(route => (
    <Route
      key={route.id}
      path={route.path}
      component={route.component}
      exact
    />
  ));
  return (
    <Container
      className={style.container}
    >
      <Switch>
        {allRoutes}
      </Switch>
    </Container>
  );
}

export default Routes;
