# Employee Explorer

This project uses to find the Employee and the list of subordinates from the user.

## Scipts

### `npm start`

Start the project on :

```
localhost:3000/

```

### `npm test`


## Usage

### `root/`
this endpoint will show the home page with search form to find the employee overview

### `root/{employeeName}`
this endpoint will show the employee overview, including the sub-ordinates from the employee.

## External Library

 - [Material UI](https://material-ui.com/)
 - [Axios](https://www.npmjs.com/package/axios)
 - [React Router Dom](https://reactrouter.com/)
